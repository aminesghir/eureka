FROM openjdk:13-jdk-alpine
VOLUME /tmp 
ADD /target/*.jar appEureka.jar 
CMD ["java","-jar","/appEureka.jar"]
EXPOSE 8080

